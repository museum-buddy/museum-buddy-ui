export class NumberRange {
  start!: number;
  end!: number;
}

export class NumberUtil {
  // Given an array of possibly sequential numbers, extract ranges out of them.
  static extractTimeRanges(nums: number[] | string): NumberRange[] {
    if (typeof nums === "string") {
      const ns: number[] = [];
      nums.split(",").forEach((n) => {
        if (n !== "") {
          ns.push(parseInt(n, 10));
        }
      });
      nums = ns;
    }
    const ranges: NumberRange[] = [new NumberRange()];
    let prev = -2;
    for (let n = 0; n < nums.length; n++) {
      const num = nums[n];
      if (prev == -2) {
        // Special case
        ranges[ranges.length - 1].start = num;
      } else if (num != prev + 1 && num != 0 && prev != 23) {
        // End a range and start a new range
        ranges[ranges.length - 1].end = prev;
        const r = new NumberRange();
        r.start = num;
        ranges.push(r);
      }
      prev = num;
    }
    // failsafe
    ranges[ranges.length - 1].end = prev;
    return ranges;
  }

  // Given an array of possibly sequential numbers, extract ranges out of them.
  static extractMonthRanges(nums: number[]): NumberRange[] {
    const ranges: NumberRange[] = [new NumberRange()];
    let prev = -2;
    for (let n = 0; n < nums.length; n++) {
      const num = nums[n];
      if (prev == -2) {
        // Special case
        ranges[ranges.length - 1].start = num;
      } else if (num != prev + 1 && num != 1 && prev != 12) {
        // End a range and start a new range
        ranges[ranges.length - 1].end = prev;
        const r = new NumberRange();
        r.start = num;
        ranges.push(r);
      }
      prev = num;
    }
    // failsafe
    ranges[ranges.length - 1].end = prev;
    return ranges;
  }

  static triBool(n: number): boolean | undefined {
    switch (n) {
      case 2:
        return true;
      case 0:
        return false;
      case 1:
      default:
        return undefined;
    }
  }
}
