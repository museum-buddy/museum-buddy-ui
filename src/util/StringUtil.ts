export class StringUtil {
  static appendParamMap(p: string, params: Record<string, unknown>): string {
    let joiner = p.indexOf("?") > -1 ? "&" : "?";
    Object.entries(params).forEach(([k, v]) => {
      if (v != undefined) {
        if (v instanceof Array) {
          v.forEach((vi) => {
            p = p + joiner + k + "=" + vi.toString();
          });
        } else {
          p = p + joiner + k + "=" + (v as object).toString();
        }
        joiner = "&";
      }
    });

    return p;
  }

  static toKebabCase(str: string): string {
    return str
      .replace(/([A-Z])([A-Z])/g, "$1-$2")
      .replace(/([a-z])([A-Z])/g, "$1-$2")
      .replace(/[\s_:]+/g, "-")
      .toLowerCase();
  }

  // kebabs an object into a kebab map
  static kebab(o: any): Record<string, unknown> {
    const ret: Record<string, unknown> = {};
    Object.keys(o).forEach((key) => {
      const nuKey = this.toKebabCase(key);
      const v = o[key];
      if (v instanceof Object) {
        ret[nuKey] = this.kebab(v as Record<string, unknown>);
      } else {
        ret[nuKey] = v;
      }
    });
    return ret;
  }

  static splinter(s: string): number[] {
    const ret: number[] = [];
    const ss = s.split(",");
    ss.forEach((ssi: string) => {
      const i = parseInt(ssi, 10);
      if (!isNaN(i)) {
        ret.push(i);
      }
    });
    return ret;
  }
}
