import { NumberUtil } from "./NumberUtil";

export class DateUtil {
  static monthRanges(months: number[]): string[] {
    const ranges = NumberUtil.extractMonthRanges(months);
    const stranges: string[] = [];
    // Check for all year
    if (ranges.length == 1 && ranges[0].start == 1 && ranges[0].end == 12) {
      return ["All Year"];
    } else {
      ranges.forEach((range) => {
        stranges.push(this.humanReadableDateRange(range.start, range.end, 3));
      });
    }
    return stranges;
  }

  static humanReadableDateRange(
    month1: number,
    month2: number,
    lim?: number
  ): string {
    let m1 = this.humanMonth(month1);
    let m2 = this.humanMonth(month2);

    if (m1 == m2) {
      return m1;
    } else if (lim != null) {
      m1 = m1.substr(0, lim);
      m2 = m2.substr(0, lim);
    }
    return m1 + " - " + m2;
  }

  static humanMonth(month: number): string {
    switch (month) {
      case 1:
        return "January";
      case 2:
        return "February";
      case 3:
        return "March";
      case 4:
        return "April";
      case 5:
        return "May";
      case 6:
        return "June";
      case 7:
        return "July";
      case 8:
        return "August";
      case 9:
        return "September";
      case 10:
        return "October";
      case 11:
        return "November";
      case 12:
        return "December";
    }
    return "";
  }

  static hourRanges(hours: number[]): string[] {
    const ranges = NumberUtil.extractTimeRanges(hours);
    const stranges: string[] = [];
    // Check for all day
    if (ranges.length == 1 && ranges[0].start == 0 && ranges[0].end == 23) {
      stranges.push("All Day");
    } else {
      ranges.forEach((range) => {
        stranges.push(this.humanReadableTimeRange(range.start, range.end));
      });
    }
    return stranges;
  }

  static humanReadableTimeRange(hour1: number, hour2: number): string {
    const h1 = this.humanHour(hour1);
    // For this to be inclusive, add one hour
    const h2 = this.humanHour(hour2 + 1);

    if (h1 == h2) {
      return h1;
    }
    return h1 + " - " + h2;
  }

  static humanHour(h: number): string {
    let tod = "AM";
    if (h > 11) {
      tod = "PM";
    }
    if (h > 12) {
      h -= 12;
    }
    return h + tod;
  }

  static parseDate(t: string): Date {
    const d = new Date();
    try {
      let dt = t.split(" ");
      if (dt.length == 1) {
        dt = t.split("T");
      }
      const dmy = dt[0].split("-");
      const ttz = dt[1].split("-");
      const hms = ttz[0].split(":");
      const s = parseInt(hms[2], 10);
      const m = parseInt(hms[1], 10);
      let h = parseInt(hms[0], 10);
      let D = parseInt(dmy[2], 10);
      const M = parseInt(dmy[1], 10);
      const y = parseInt(dmy[0], 10);

      if (
        isNaN(s) ||
        isNaN(m) ||
        isNaN(h) ||
        isNaN(D) ||
        isNaN(M) ||
        isNaN(y)
      ) {
        console.error("bad date");
        return new Date();
      }
      // Determine over/underflow
      if (h == 0) {
        h = 24;
        D = D - 1;
      }

      d.setSeconds(s);
      d.setMinutes(m);
      d.setHours(h);
      d.setDate(D);
      d.setMonth(M);
      d.setFullYear(y);
    } catch (e) {
      console.error("couldn't parse date: " + e);
      return new Date();
    }

    return d;
  }

  static formatDate(date: Date, excludeTimezone = false): string {
    let d = date.getFullYear() + "";
    d += "-";
    if (date.getMonth() < 10) {
      d += "0";
    }
    d += date.getMonth();
    d += "-";
    if (date.getDate() < 10) {
      d += "0";
    }
    d += date.getDate();
    d += " ";
    if (date.getHours() < 10) {
      d += "0";
    }
    d += date.getHours();
    d += ":";
    if (date.getMinutes() < 10) {
      d += "0";
    }
    d += date.getMinutes();
    d += ":";
    if (date.getSeconds() < 10) {
      d += "0";
    }
    d += date.getSeconds();
    d += "-";
    if (excludeTimezone) {
      d += "0000";
    } else {
      const tz = date.getTimezoneOffset() / 60;
      if (tz < 10) {
        d += "0";
      }
      d += tz;
      d += "00";
    }
    return d;
  }

  static getTimezoneOffset(): number {
    return new Date().getTimezoneOffset() / 60;
  }
}
