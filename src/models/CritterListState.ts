export type CritterListState = {
  caught: number;
  donated: number;
  seasonal: number;
  catchable: number;
};
