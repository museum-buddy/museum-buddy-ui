import type {
  NHArt,
  NHFish,
  NHFossil,
  NHInsect,
  NHObject,
  NHSeaCritter,
} from "@/models/NH";
import type { PaginationObject } from "@/models/PagingationObject";
import { DateUtil } from "@/util/DateUtil";
import { useBaseUrlStore, useDataStore, useOptionsStore } from "@/store";
import { StringUtil } from "@/util/StringUtil";
import { MuseumObjectType } from "@/clients/data-client";
import { useGtm } from "vue-gtm";

export type CritterFilters = {
  caught?: boolean;
  donated?: boolean;
  catchable?: boolean;
  seasonal?: boolean;
  sort?: string;
  dir?: string;
  limit?: number;
  offset?: number;
};

export class MuseumBuddyAPI {
  private options = useOptionsStore();
  private data = useDataStore();
  private baseUrl = useBaseUrlStore();
  private gtm = useGtm();

  async getCritters<T extends NHObject>(
    critterType: MuseumObjectType,
    filters?: CritterFilters
  ): Promise<PaginationObject<T>> {
    const params: Record<string, unknown> = {
      ...filters,
      hemisphere: this.options.client.getHemisphere(),
      "time-offset": this.options.client.getDataOffset(),
    };

    if (filters?.caught != null) {
      params["caught-id"] = this.data.client.getCaughtObjects(critterType);
    }
    if (filters?.donated != null) {
      params["donated-id"] = this.data.client.getDonatedObjects(critterType);
    }

    this.gtm?.trackEvent({ event: "get-critters", category: critterType });

    const resp = await fetch(
      StringUtil.appendParamMap(
        `${this.baseUrl.base}/nh/${critterType}/`,
        params
      )
    );

    return await resp.json();
  }

  async getCritter<T extends NHObject>(
    critterType: MuseumObjectType,
    id: string
  ): Promise<T> {
    const resp = await fetch(`${this.baseUrl.base}/nh/${critterType}/${id}`);
    return await resp.json();
  }

  async getOffset(date: Date): Promise<string> {
    const params = { "system-time": DateUtil.formatDate(date, true) };
    const resp = await fetch(
      StringUtil.appendParamMap(`${this.baseUrl.base}/time-offset`, params)
    );
    const parsed: { "time-offset": string } = await resp.json();
    return parsed["time-offset"];
  }

  async getTime(o: string): Promise<Date> {
    const params = { "time-offset": o };
    const resp = await fetch(
      StringUtil.appendParamMap(`${this.baseUrl.base}/time-from-offset`, params)
    );
    const parsed: { time: string } = await resp.json();
    return DateUtil.parseDate(parsed.time);
  }

  async getInsects(
    filters?: CritterFilters
  ): Promise<PaginationObject<NHInsect>> {
    return this.getCritters<NHInsect>(MuseumObjectType.INSECT, filters);
  }

  async getInsect(identifier: string): Promise<NHInsect> {
    return this.getCritter<NHInsect>(MuseumObjectType.INSECT, identifier);
  }

  async getFishes(filters?: CritterFilters): Promise<PaginationObject<NHFish>> {
    return this.getCritters<NHFish>(MuseumObjectType.FISH, filters);
  }

  async getFish(identifier: string): Promise<NHFish> {
    return this.getCritter<NHFish>(MuseumObjectType.FISH, identifier);
  }

  async getSeaCritters(
    filters?: CritterFilters
  ): Promise<PaginationObject<NHSeaCritter>> {
    return this.getCritters<NHSeaCritter>(
      MuseumObjectType.SEA_CRITTER,
      filters
    );
  }

  async getSeaCritter(identifier: string): Promise<NHSeaCritter> {
    return this.getCritter<NHSeaCritter>(
      MuseumObjectType.SEA_CRITTER,
      identifier
    );
  }

  async getFossils(
    filters?: CritterFilters
  ): Promise<PaginationObject<NHFossil>> {
    return this.getCritters(MuseumObjectType.FOSSIL, filters);
  }

  async getArt(filters?: CritterFilters): Promise<PaginationObject<NHArt>> {
    return this.getCritters(MuseumObjectType.ART, filters);
  }
}
