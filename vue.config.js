module.exports = {
  css: {
    loaderOptions: {
      sass: {
        prependData: `@import "@/styles/_variables.scss";`,
      },
    },
  },
  publicPath: "/",
  pwa: {
    name: "Museum Buddy",
    manifestOptions: {
      icons: [
        {
          src: "img/icons/maskable_icon_x512.png",
          sizes: "512x512",
          type: "image/png",
          purpose: "any maskable",
        },
        { src: "img/icons/ms-icon-310x310.png", sizes: "310x310" },
        {
          src: "img/icons/android-icon-192x192.png",
          sizes: "192x192",
        },
        { src: "img/icons/android-icon-72x72.png", sizes: "72x72" },
        {
          src: "img/icons/favicon-32x32.png",
          sizes: "32x32",
        },
        { src: "img/icons/favicon-16x16.png", sizes: "16x16" },
      ],
    },
    iconPaths: {
      favicon32: "img/icons/favicon-32x32.png",
      favicon16: "img/icons/favicon-16x16.png",
      appleTouchIcon: "img/icons/apple-icon-180x180.png",
      maskIcon: "img/icons/android-icon-192x192.png",
      msTileImage: "img/icons/ms-icon-310x310.png",
    },
  },
};
