import type { CritterListState } from "./CritterListState";
import type { MuseumObjectType } from "@/clients/data-client";

export type CritterData = {
  name: MuseumObjectType;
  caught: number[];
  donated: number[];
  state: CritterListState;
};
