export type NHObject = {
  id: number;
  name: string;
  sellValue: number;
  type: string;
};

export type NHCritter = NHObject & {
  critterId: number;
  location: string;
  needsRain: boolean;
  hours: number[];
  northMonths: number[];
  southMonths: number[];
};

export type NHInsect = NHCritter;

export type NHFish = NHCritter & {
  shadowSize: string;
  hasFin: boolean;
};

export type NHSeaCritter = NHCritter & {
  shadowSize: string;
  speed: number;
};

export type NHFossil = NHObject;
export type NHArt = NHObject & {
  realName: string;
  realArtist: string;
  forgeryHints: string;
};

export function catchable(o: NHCritter | NHObject): boolean {
  return "critterId" in o;
}

export function speedName(o: NHSeaCritter): string {
  switch (o.speed) {
    case 1:
      return "Slow";
    case 2:
      return "Average";
    case 3:
      return "Fast";
    default:
      return "Stationary";
  }
}
