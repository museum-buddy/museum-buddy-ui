import { DataClient } from "@/clients/data-client";
import { MuseumBuddyAPI } from "@/clients/museum-buddy-api";
import { OptionClient } from "@/clients/option-client";
import { defineStore } from "pinia";

export const useBaseUrlStore = defineStore("baseUrl", {
  state: () => ({ base: "http://localhost:8080/v1" }),
  actions: {
    set(b: string) {
      this.base = b;
    },
  },
});

export const useOptionsStore = defineStore("options", {
  state: () => ({ client: new OptionClient() }),
});

export const useDataStore = defineStore("data", {
  state: () => ({ client: new DataClient() }),
});

export const useApiStore = defineStore("api", {
  state: () => ({ client: new MuseumBuddyAPI() }),
});
