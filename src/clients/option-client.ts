export class OptionClient {
  getHemisphere(): string {
    const h = localStorage.getItem("hemisphere");
    if (h == null) {
      return "north";
    }
    return h;
  }

  setHemisphere(h: string) {
    localStorage.setItem("hemisphere", h);
  }

  saveTimezoneOffset() {
    const o = -1 * new Date().getTimezoneOffset();
    localStorage.setItem("time-zone-minutes", `${o}`);
  }

  getSystemOffset(): string {
    return localStorage.getItem("time-offset") as string;
  }

  setSystemOffset(o: string) {
    localStorage.setItem("time-offset", o);
  }

  clearSystemOffset() {
    localStorage.removeItem("time-offset");
  }

  getDataOffset(): string {
    let o = this.getSystemOffset();
    let h = 0,
      m = 0;
    if (o) {
      if (o.indexOf("h") >= 0) {
        const sp = o.split("h");
        h = parseInt(sp[0], 10);
        o = sp[1];
      }
      if (o.indexOf("m") >= 0) {
        const sp = o.split("m");
        m = parseInt(sp[0], 10);
      }
    } else {
      m = parseInt(localStorage.getItem("time-zone-minutes") ?? "0", 10);
    }
    m += 60 * h;
    return m + "m";
  }
}
