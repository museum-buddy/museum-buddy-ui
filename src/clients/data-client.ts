import { useGtm } from "vue-gtm";

export enum MuseumObjectType {
  FISH = "fish",
  INSECT = "insect",
  SEA_CRITTER = "sea-critter",
  FOSSIL = "fossil",
  ART = "art",
}

export class DataClient {
  private gtm = useGtm();

  getCaughtObjects(moType: MuseumObjectType): number[] {
    const c = localStorage.getItem(`caught-${moType}`);
    if (c != null) {
      return JSON.parse(c);
    }
    return [];
  }

  setCaughtObjects(moType: MuseumObjectType, list: number[]) {
    localStorage.setItem(`caught-${moType}`, JSON.stringify(list));
  }

  toggleCatchObject(
    moType: MuseumObjectType,
    id: number,
    callback?: () => void
  ) {
    if (this.isCaught(moType, id)) {
      this.uncatchObject(moType, id);
    } else {
      this.catchObject(moType, id);
    }
    if (callback) {
      callback();
    }
  }

  catchObject(moType: MuseumObjectType, id: number) {
    const c = this.getCaughtObjects(moType);
    c.push(id);
    localStorage.setItem(`caught-${moType}`, JSON.stringify(c));
    this.gtm?.trackEvent({ event: "catch", category: moType, value: id });
  }

  uncatchObject(moType: MuseumObjectType, id: number) {
    const c = this.getCaughtObjects(moType);
    const cridx = c.indexOf(id);
    if (cridx > -1) {
      c.splice(cridx);
    }
    localStorage.setItem(`caught-${moType}`, JSON.stringify(c));
    this.gtm?.trackEvent({ event: "uncatch", category: moType, value: id });
  }

  isCaught(moType: MuseumObjectType, id: number): boolean {
    return this.getCaughtObjects(moType).indexOf(id) > -1;
  }

  getDonatedObjects(moType: MuseumObjectType): number[] {
    const d = localStorage.getItem(`donated-${moType}`);
    if (d != null) {
      return JSON.parse(d);
    }
    return [];
  }

  setDonatedObjects(moType: MuseumObjectType, list: number[]) {
    localStorage.setItem(`donated-${moType}`, JSON.stringify(list));
  }

  toggleDonateObject(
    moType: MuseumObjectType,
    id: number,
    callback?: () => void
  ) {
    if (this.isDonated(moType, id)) {
      this.undonateObject(moType, id);
    } else {
      this.donateObject(moType, id);
    }
    if (callback) {
      callback();
    }
  }

  donateObject(moType: MuseumObjectType, id: number) {
    const d = this.getDonatedObjects(moType);
    d.push(id);
    localStorage.setItem(`donated-${moType}`, JSON.stringify(d));
    this.gtm?.trackEvent({ event: "donate", category: moType, value: id });
  }

  undonateObject(moType: MuseumObjectType, id: number) {
    const d = this.getDonatedObjects(moType);
    const cridx = d.indexOf(id);
    if (cridx > -1) {
      d.splice(cridx);
    }
    localStorage.setItem(`donated-${moType}`, JSON.stringify(d));
    this.gtm?.trackEvent({ event: "undonate", category: moType, value: id });
  }

  isDonated(moType: MuseumObjectType, id: number): boolean {
    return this.getDonatedObjects(moType).indexOf(id) > -1;
  }

  saveFilterState(
    critter: MuseumObjectType,
    caught: number,
    donated: number,
    catchable: number,
    seasonal: number
  ) {
    localStorage.setItem(
      `${critter}-filters`,
      JSON.stringify({
        caught: caught,
        donated: donated,
        catchable: catchable,
        seasonal: seasonal,
      })
    );
  }

  loadFilterState(moType: MuseumObjectType): {
    caught: number;
    donated: number;
    catchable: number;
    seasonal: number;
  } {
    const l = localStorage.getItem(`${moType}-filters`);
    if (l != null) {
      return JSON.parse(l);
    }
    return { caught: 1, donated: 1, catchable: 1, seasonal: 1 };
  }
}
