import { createRouter, createWebHistory } from "vue-router";
import Home from "../views/HomePage.vue";
import Fish from "../views/FishPage.vue";
import Bugs from "../views/InsectPage.vue";
import Sea from "../views/SeaCrittersPage.vue";
import Fossils from "../views/FossilPage.vue";
import Art from "../views/ArtPage.vue";
import About from "../views/AboutPage.vue";
import NotFound from "../views/NotFound.vue";

const routes = [
  { path: "/", name: "Home", component: Home },
  { path: "/fish", name: "Fish", component: Fish },
  { path: "/bugs", name: "Bugs", component: Bugs },
  { path: "/sea", name: "Sea", component: Sea },
  { path: "/fossils", name: "Fossils", component: Fossils },
  { path: "/art", name: "Art", component: Art },
  { path: "/about", name: "About", component: About },
  { path: "/:pathMatch(.*)*", name: "404", component: NotFound },
];

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: routes,
});

export default router;
