import { fileURLToPath, URL } from "node:url";

import { defineConfig } from "vite";
import vue from "@vitejs/plugin-vue";
import type { VitePWAOptions } from "vite-plugin-pwa";
import { VitePWA } from "vite-plugin-pwa";

const pwaOptions: Partial<VitePWAOptions> = {
  base: "/",
  includeAssets: ["/favicon.ico"],
  manifest: {
    background_color: "#42b983",
    description:
      "An application to track your Island Museum progress on Animal Crossing: New Horizons!",
    display: "standalone",
    icons: [
      {
        src: "img/icons/maskable_icon_x512.png",
        sizes: "512x512",
        type: "image/png",
        purpose: "any maskable",
      },
      { src: "img/icons/ms-icon-310x310.png", sizes: "310x310" },
      {
        src: "img/icons/android-icon-192x192.png",
        sizes: "192x192",
      },
      { src: "img/icons/android-icon-72x72.png", sizes: "72x72" },
      {
        src: "img/icons/favicon-32x32.png",
        sizes: "32x32",
      },
      { src: "img/icons/favicon-16x16.png", sizes: "16x16" },
    ],
    name: "Museum Buddy",
    short_name: "MuseumBuddy",
    start_url: "/",
    theme_color: "#42b983",
  },
  registerType: "autoUpdate",
  strategies: "generateSW",
  workbox: { globPatterns: ["**/*.{js,css,html,ico,png,svg}"] },
};

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [vue(), VitePWA(pwaOptions)],
  resolve: {
    alias: {
      "@": fileURLToPath(new URL("./src", import.meta.url)),
    },
  },
  css: {
    preprocessorOptions: {
      scss: {
        additionalData: ` // just variables loaded globally
          @import "./src/styles/_variables";
        `,
      },
    },
  },
  appType: "spa",
});
