import { shallowMount } from "@vue/test-utils";
import Art from "@/components/ArtPage.vue";

describe("App.vue", () => {
  it("renders", () => {
    const wrapper = shallowMount(Art, {
      propsData: {
        art: {
          ID: 1,
          Name: "boody",
          SellValue: 5000,
          Type: "Art",
          RealName: "Shrimbo",
          RealArtist: "Dali",
          ForgeryHints: [],
        },
      },
    });
    expect(wrapper.text()).toContain("boody");
  });
});
