export type PaginationObject<T> = {
  offset: number;
  limit: number;
  total: number;
  items: T[];
};
