import type { CritterData } from "@/models/CritterData";

export type PortableData = {
  critters: CritterData[];
  hemisphere: string;
  systemOffset: string;
};
