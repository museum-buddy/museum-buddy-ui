import { createApp } from "vue";
import { createPinia } from "pinia";
import { FontAwesomeIcon } from "@fortawesome/vue-fontawesome";
import { library } from "@fortawesome/fontawesome-svg-core";

//region icons
import {
  faBars,
  faCheck,
  faChevronLeft,
  faChevronRight,
  faDownload,
  faFish,
  faMeteor,
  faPalette,
  faPause,
  faPlay,
  faQuestion,
  faSave,
  faSpinner,
  faTimes,
  faUpload,
} from "@fortawesome/free-solid-svg-icons";
//endregion
import router from "./router";
import App from "./App.vue";
import SeaCritterList from "@/components/museum-object-lists/SeaCritterList.vue";
import SvgBells from "@/icons/svg-bells.vue";
import SvgFishingHook from "@/icons/svg-fishing-hook.vue";
import SvgInsect from "@/icons/svg-insect.vue";
import SvgHelmet from "@/icons/svg-helmet.vue";
import SvgIsland from "@/icons/svg-island.vue";
import SvgNet from "@/icons/svg-net.vue";
import SvgOwl from "@/icons/svg-owl.vue";
import SvgSharkFin from "@/icons/svg-shark-fin.vue";
import MuseumObject from "@/components/museum-object/MuseumObject.vue";
import BooleanIndicator from "@/components/primitives/BooleanIndicator.vue";
import MuseumCollapse from "@/components/primitives/MuseumCollapse.vue";
import MuseumFooter from "@/components/primitives/MuseumFooter.vue";
import MuseumModal from "@/components/primitives/MuseumModal.vue";
import MuseumPaginator from "@/components/primitives/MuseumPaginator.vue";
import TriStateToggle from "@/components/primitives/TriStateToggle.vue";
import MuseumConfig from "@/components/primitives/MuseumConfig.vue";
import MuseumObjectOptions from "@/components/museum-object/MuseumObjectOptions.vue";
import MuseumObjectFilters from "@/components/museum-object-lists/MuseumObjectFilters.vue";
import FossilList from "@/components/museum-object-lists/FossilList.vue";
import FishList from "@/components/museum-object-lists/FishList.vue";
import ArtList from "@/components/museum-object-lists/ArtList.vue";
import InsectList from "@/components/museum-object-lists/InsectList.vue";
import SeaCritterProps from "@/components/museum-object/SeaCritterProps.vue";
import FishProps from "@/components/museum-object/FishProps.vue";
import InsectProps from "@/components/museum-object/InsectProps.vue";
import ArtProps from "@/components/museum-object/ArtProps.vue";
import { useBaseUrlStore } from "@/store";
import { createGtm } from "vue-gtm";

library.add(
  faCheck,
  faTimes,
  faPause,
  faPlay,
  faQuestion,
  faSpinner,
  faDownload,
  faUpload,
  faChevronLeft,
  faChevronRight,
  faPalette,
  faFish,
  faBars,
  faMeteor,
  faSave
);

const app = createApp(App);

app.use(createPinia());
app.use(router);

//region icons
app.component("fa-icon", FontAwesomeIcon);
app.component("svg-bells", SvgBells);
app.component("svg-fishing-hook", SvgFishingHook);
app.component("svg-helmet", SvgHelmet);
app.component("svg-insect", SvgInsect);
app.component("svg-island", SvgIsland);
app.component("svg-net", SvgNet);
app.component("svg-owl", SvgOwl);
app.component("svg-shark-fin", SvgSharkFin);
//endregion
//region critters
app.component("MuseumObject", MuseumObject);
app.component("MuseumObjectOptions", MuseumObjectOptions);
app.component("MuseumObjectFilters", MuseumObjectFilters);
app.component("SeaCritterList", SeaCritterList);
app.component("FossilList", FossilList);
app.component("FishList", FishList);
app.component("InsectList", InsectList);
app.component("ArtList", ArtList);
app.component("SeaCritterProps", SeaCritterProps);
app.component("FishProps", FishProps);
app.component("InsectProps", InsectProps);
app.component("ArtProps", ArtProps);
//endregion
//region primitives
app.component("BooleanIndicator", BooleanIndicator);
app.component("MuseumConfig", MuseumConfig);
app.component("MuseumCollapse", MuseumCollapse);
app.component("MuseumFooter", MuseumFooter);
app.component("MuseumModal", MuseumModal);
app.component("MuseumPaginator", MuseumPaginator);
app.component("TriStateToggle", TriStateToggle);
//endregion

const baseUrlStore = useBaseUrlStore();

fetch("/config/config.json")
  .then((resp) =>
    resp.json().then((j) => {
      baseUrlStore.set(j.base);
      if (j.gtmId) {
        app.use(
          createGtm({
            id: j.gtmId,
            vueRouter: router,
          })
        );
      }
    })
  )
  .catch((e) => console.error(`Failed to load config: ${e}`))
  .finally(() => app.mount("#app"));
